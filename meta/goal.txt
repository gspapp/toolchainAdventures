My main goal is to have a complete GNU toolchain to start building the 
rest of the GNU operating system tools. The first step of this long 
journey is compiling GCC. After GCC we can start compiling the rest of 
the GNU tools and the few third party programs that an operating system 
needs.

We first have to reach a state in which we can compile GCC 2.95.3 and 
from that we can compile GCC 4.7 and then finally the latest GCC.
