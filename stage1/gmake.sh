#!/bin/sh
. ./conf

if [ ! -r "${SRCPREFIX}/gmake-${GMAKEVERSION}.tar.gz" ] ; then
	if [ -x $(which wget) ] ; then
		wget "${FTPLSITEGNU}/make/make-{$GMAKEVERSION}.tar.gz" -O "${SRCPREFIX}/gmake-${GMAKEVERSION}.tar.gz"
	elif [ -x $(which curl) ] ; then
		curl -L "${FTPLSITEGNU}/make/make-{$GMAKEVERSION}.tar.gz" -o "${SRCPREFIX}/gmake-${GMAKEVERISON}.tar.gz"
	else
		echo "put gmake-${GMAKEVERSION}.tar.gz in ${SRCPREFIX} and continue"
	fi
fi

cd "$SRCPREFIX"
gzip -d "gmake-${GMAKEVERSION}.tar.gz"
tar xvf "gmake-${GMAKEVERSION}.tar"
cd "make-${GMAKEVERSION}"
./configure --prefix="${PREFIX}" --enable-year2038 CFLAGS="${CFLAGS}" CC="${CC}"
if [ -x "$(which make)" ] ;then
	make "${MAKEOPTS}"
	make install
else
	./build.sh
	./make install
fi
